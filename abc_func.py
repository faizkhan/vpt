import scipy.integrate as integrate #integration library
import scipy.constants as spc #scientific constants
import matplotlib
import matplotlib.pyplot as plt
import sympy
import mpmath
import numpy as np
import scipy
import optparse as op

#matplotlib.rcParams['axes.titlesize'] = 24
matplotlib.rcParams['font.size'] = 24

emass = spc.physical_constants["electron mass energy equivalent in MeV"][0]

#Constants declared here
#potential = 1
#temp = 10
#beta = 1/temp

#Implementation of a()
def a( m_e, b, mu ):
  if mu > 0:
    return np.log(1 + np.exp( -(m_e + mu)*b ))
  elif mu < 0:
    return np.log(1 + np.exp( -(m_e - mu)*b ))

#Implementation of b()
def b( m_e, b, mu, debug=False):
  if mu > 0:
    #NOTE: using mpmath methods may lead to higher precision, below is a test
    return mpmath.nsum( lambda n: \
      mpmath.fmul( \
        mpmath.power(-1,n),\
        mpmath.fmul( \
          mpmath.exp( \
            mpmath.fneg( \
             mpmath.fmul(
             mpmath.fmul(n, b), mu ) ) ), mpmath.ei( -n*b*emass ) ) ), \
     [1, mpmath.inf], verbose=debug)
  elif mu < 0:
    return mpmath.nsum( lambda n: mpmath.power(-1,n) * mpmath.exp( +(n * b * mu) ) * mpmath.ei( -n*b*emass ), [1, mpmath.inf], verbose=debug)

def cdf( m_e, b, mu, pwr, debug=False):
  if mu > 0:
    return mpmath.nsum( lambda n: ( mpmath.power(-1,n) / mpmath.power(n,pwr) ) * mpmath.exp( -n * ( m_e + mu) * b ), [1, mpmath.inf], verbose=debug)
  elif mu < 0:
    return mpmath.nsum( lambda n: ( mpmath.power(-1,n) / mpmath.power(n,pwr) ) * mpmath.exp( -n * ( m_e - mu) * b ), [1, mpmath.inf], verbose=debug)

def g( m_e, b, mu, debug=False):
  if mu > 0:
    return mpmath.nsum( lambda n: mpmath.power(-1,n) * ( ( b * n * mpmath.ei( -n*b*emass ) ) + (mpmath.exp( -1 * n * m_e * b ) / m_e ) ) * mpmath.exp( -1 * n * mu * b ), [1, mpmath.inf], verbose=debug)
  elif mu < 0:
    return mpmath.nsum( lambda n: mpmath.power(-1,n) * ( ( b * n * mpmath.ei( -n*b*emass ) ) + (mpmath.exp( -1 * n * m_e * b ) / m_e ) ) * mpmath.exp( +1 * n * mu * b ), [1, mpmath.inf], verbose=debug)

def h( m_e, b, mu, debug=False):
  if mu > 0:
    return (-1 * mpmath.nsum( lambda n: mpmath.power(-1,n) * ( ( mpmath.power(b,2) * mpmath.power(n,2) * (1/2) * mpmath.ei( -n*b*emass ) ) + n * b * (mpmath.exp( -1 * n * m_e * b ) / m_e ) ) * mpmath.exp( -1 * n * mu * b ), [1, mpmath.inf], verbose=debug) )
  elif mu < 0:
    return (-1 * mpmath.nsum( lambda n: mpmath.power(-1,n) * ( ( mpmath.power(b,2) * mpmath.power(n,2) * (1/2) * mpmath.ei( -n*b*emass ) ) + n * b * (mpmath.exp( -1 * n * m_e * b ) / m_e ) ) * mpmath.exp( +1 * n * mu * b ), [1, mpmath.inf], verbose=debug) )


def execute(p,t, frame=None, type_=None):

  potential = p
  temp = t
  beta = 1/temp
  print("\nPotential: ", potential)
  print("Temperature: ", temp)
  print("Beta: ", beta)

  if type_ == 0:
    print("a: ", a( emass, beta, potential ))
    if frame != None:
      frame += [ a( emass, beta, potential ) ]
      #print(frame)

  if type_ == 1:
    print("b: ", b( emass, beta, potential ))
    if frame != None:
      frame += [ b( emass, beta, potential ) ]
      #print(frame)

  if type_ == 2:
    print("c: ", cdf( emass, beta, potential, 2))
    if frame != None:
      frame += [ cdf( emass, beta, potential, 2) ]

  if type_ == 3:
    print("d: ", cdf( emass, beta, potential, 3))
    if frame != None:
      frame += [ cdf( emass, beta, potential, 3) ]

  if type_ == 4:
    print("f: ", cdf( emass, beta, potential, 4))
    if frame != None:
      frame += [ cdf( emass, beta, potential, 4) ]

  if type_ == 5:
    print("g: ", g( emass, beta, potential))
    if frame != None:
      frame += [ g( emass, beta, potential) ]

  if type_ == 6:
    print("h: ", h( emass, beta, potential))
    if frame != None:
      frame += [ h( emass, beta, potential) ]


def graph_T_gt_u():

  for i in range(0, 7):
    aframe = []
    for t in range(1,11):
      #fix potential to be << T
      p = 0.1

      execute(p, t, frame=aframe, type_=i)

    trange = np.arange(1,11,1)
    aframe = np.array(aframe)
    fix, ax = plt.subplots()
    ax.plot(trange,aframe)
    ax.set(xlabel='Temperature (K * 10^10)', ylabel="Function " + letterkey[str(i)] + '()', title= "Function " + letterkey[str(i)] + '() when T >> μ')
    ax.grid()
    plt.show()

letterkey = { '0': 'a', '1': 'b', '2': 'c', '3' : 'd', '4':'f', '5':'g', '6': 'h' }

def graph_u_gt_T():

  for i in range(0, 7):
    aframe = []
    for p in range(20,101):
      #fix temp to be << mu
      t = 2

      execute(p, t, frame=aframe, type_=i)

    trange = np.arange(20,101,1)
    aframe = np.array(aframe)
    fix, ax = plt.subplots()
    ax.plot(trange,aframe)
    ax.set(xlabel='Potential (J)', ylabel="Function " + letterkey[str(i)] + '()', title= "Function " + letterkey[str(i)] + '() when μ >> T')
    ax.grid()
    plt.show()

def rawdata_ugtt():
  for i in range(0, 7):
    for p in range(20,101):
      #fix temp to be << mu
      t = 2
      execute(p, t, frame=None, type_=i)

def rawdata_tgtu():
  for i in range(0, 7):
    for t in range(1,11):
      #fix potential to be << T
      p = 0.1
      execute(p, t, frame=None, type_=i)

usage = "\n\n%prog command\n where 'command' is either 'ugtt', 'tgtu', or 'both'"
parser = op.OptionParser(usage)
(options,args) = parser.parse_args()
#print(options, args)

if len(args):
  if args[0] in ["UGTT", "ugtt", "both"]:
    graph_u_gt_T()

  if args[0] in ["TGTU", "tgtu", "both"]:
    graph_T_gt_u()

  if args[0] in ["rawdata"]:
    rawdata_ugtt()
    rawdata_tgtu()

  #parser.print_help()

  #Debug the sums with debug=True flag)
  #ie. h( emass, beta, potential, debug=True)
  input("\nPress Return to quit...")
