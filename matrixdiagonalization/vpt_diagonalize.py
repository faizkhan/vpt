#vacuum polarization tensor simulation

#import dependencies
import scipy.integrate as integrate #integration library
import scipy.constants as spc #scientific constants
import sympy
import numpy as np #matrix library
import random as rand

sympy.interactive.printing.init_printing(use_unicode=False, wrap_line=False)

#options
np.set_printoptions(linewidth=1000)

#declare constants
iterations = 10000 #iteration count for infinite sums - these should converge eventually
emass = spc.physical_constants["electron mass energy equivalent in MeV"][0]

#main class that deals with computation of model
class VacuumPolarizationTensor:

  #default model parameters
  lambda1 = rand.randint(300,375) * 10**-9 #nanometers
  lambda2 = rand.randint(300,375) * 10**-7 #nanometers 

  #calculate temporal angular frequency in rad/s
  omega = np.floor( (2 * spc.pi) / lambda1) 
  #calculate spatial angular frequency (wavenumber) in rad/nm
  k = np.floor( (2*spc.pi) / lambda2 )
  kx = ky = kz = np.floor(np.sqrt(k**2 / 3 ))
  #kelvin = 8 * 10**-1
  #kelvin = 1 * 10**10 #Kelvin
  kelvin=2
  beta = 1 / kelvin

  #augment kx,ky,kz with some random factor
  #kx = np.floor(k + k * (rand.randint(0,1000) / 1000))
  #ky = np.floor(k + k * (rand.randint(0,1000) / 1000))
  #kz = np.floor(k + k * (rand.randint(0,1000) / 1000))
  #TODO include check/assert that kx^2 + ky^2 + kz^2 must equal k

  #the four-wavevector K is defined as ( omega/c, kx,ky,kz) 
  #where c = v*lambda (v = temporal freq.) and omega = 2pi*v

  def __printparams(p):
    #print("\nModel parameters:\n")
    print("{:<20} {:<20} {:<20}".format(
     "omega={}".format(p.omega),
     "kelvin={}".format(p.kelvin),
     "beta={}".format(p.beta)))
    print("{:<20} {:<20} {:<20} {:<20} ".format(
     "k={}".format(p.k),
     "kx={}".format(p.kx),
     "ky={}".format(p.ky),
     "kz={}".format(p.kz)))
    print("{:<20} {:<20} {:<20} {:<20} ".format(
     "K={}".format(p.K),
     "kelvin/m_e={}".format(np.round(p.kelvin/emass,3)),
     "longitudinal={}".format(p.vpt_l),
     "transverse={}".format(p.vpt_t)))


  #routine that does computation
  def compute(p): #p is the class object holding the model parameters defined above, params is given by the caller

    #TODO 
    #convert to more accurate Decimal format
    #should have some test values to check this program against
    #   known values of pi tensor shouldⁿ0 match before extended simulations are run

    #compute longitudinal component of vpt, based on formula from "QED Plasma at Finite Temperature up to Two Loops"
    def compute_longitudinal():
        return ( (4 * spc.e**2) / spc.pi**2 ) \
          * ( 1 - ( p.omega**2 / p.k**2 ) ) \
          * ( ( 1 - (p.omega / ( 2 * p.k )) \
          * np.log( (p.omega + p.k) / (p.omega - p.k) ) ) \
          * ( emass * p.a( emass * p.beta ) / p.beta - p.c( emass * p.beta ) / p.beta**2 ) + 1/4 \
          * ( 2*emass**2 - p.omega**2 + (11*p.k**2 + 37*p.omega**2) / 72 ) * p.b( emass * p.beta ) )

    #compute transverse component of vpt, based on formula from "QED Plasma at Finite Temperature up to Two Loops"
    def compute_transverse():
        return ( (2 * spc.e**2) / spc.pi**2 ) \
          * ( ( p.omega**2/p.k**2 + (1 - p.omega**2/p.k**2 )) \
          * (p.omega / (2*p.k) ) \
          * np.log( (p.omega + p.k) / (p.omega - p.k) )  \
          * ( emass * p.a( emass * p.beta ) / p.beta - p.c( emass * p.beta ) / p.beta**2 ) + 1/8 \
          * ( 2*emass**2 - p.omega**2 + (107*p.omega**2 + 131*p.k**2) / 72 ) * p.b( emass * p.beta ) )

    if p.omega != p.k:
      #compute 4-momentum of photon
      p.K = np.floor( np.sqrt(p.omega**2 - p.k**2) )

      if p.params and p.params.longitudinal == 2:
        p.vpt_l = compute_longitudinal()
      elif p.params and p.params.longitudinal == 1:
        p.vpt_l = 1 
      else:
        p.vpt_l = 0

      if p.params and p.params.transverse == 2:
        p.vpt_t = compute_transverse()
      elif p.params and p.params.transverse == 0:
        p.vpt_t = 0
      else:
        p.vpt_t = 1

      p.__printparams()

    else:
      #if omega == k, then we don't compute anything as this would cause a lot of undefined behavior
      #this branch returns the 0 tensor
      p.K = 1
      p.vpt_l = 0
      p.vpt_t = 0

    vpt = p.construct_vpt()

    #print("\nDEBUG:\n",vpt_l, vpt_t)
    print("\nVPT:\n\n", vpt)

  #compute Masoods abc functions, based on formulae from "QED Plasma at Finite Temperature up to Two Loops"
  def a( p, mb ):
    return np.log(1 + np.exp(-mb))

  def b( p, mb ):
    bsum = 0
    for n in range(1,iterations+1):
      bsum += (-1)**n * integrate.quad( lambda t: -np.exp(-t)/t, n*mb, np.inf)[0]
    return bsum

  def c( p, mb ):
    csum = 0
    for n in range(1,iterations+1):
      csum += (-1)**n * (np.exp(-n * mb)/n**2)
    return csum

  #compute vpt tensor, based on formula from "QED Plasma at Finite Temperature up to Two Loops"
  def construct_vpt(p):
    k, omega, kx, vpt_l, K, vpt_t, ky, kz = sympy.symbols('k omega k_x vpt_l K vpt_t k_y k_z')
    a = sympy.Matrix([
        # Row 1
        [ (-k**2 * vpt_l) / K**2,                          #Column 1
          (-complex(0,1) * omega * kx * vpt_l) / K**2,   #Column 2
          (-complex(0,1) * omega * ky * vpt_l) / K**2,   #Column 3 
          (-complex(0,1) * omega * kz * vpt_l) / K**2 ], #Column 4 

        # Row 2
        [ (-complex(0,1) * omega * kx * vpt_l) / K**2, 
          (-1 - ( kx**2 / k**2 ) ) * vpt_t + ( omega**2 * kx**2 * vpt_l ) / ( k**2 * K**2 ), 
          ( -kx * ky * vpt_t ) / k**2 + ( omega**2 * kx * ky * vpt_l ) / ( k**2 * K**2 ), 
          ( -kx * kz * vpt_t ) / k**2 + ( omega**2 * kx * kz * vpt_l ) / ( k**2 * K**2 ) ],

        # Row 3
        [ (-complex(0,1) * omega * ky * vpt_l) / K**2, 
          ( -kx * ky * vpt_t ) / k**2 + ( omega**2 * kx * ky * vpt_l ) / ( k**2 * K**2 ),
          ( -1 - ( ky**2/ k**2 ) ) * vpt_t + ( omega**2 * ky**2 * vpt_l ) / ( k**2 * K**2 ), 
          (( -ky * kz * vpt_t ) / k**2) + ( omega**2 * ky * kz * vpt_l ) / ( k**2 * K**2 ) ],

        # Row 4
        [ (-complex(0,1) * omega * kz * vpt_l) / k**2, 
          ( -kx * kz * vpt_t ) / k**2 + ( omega**2 * kx * kz * vpt_l ) / ( k**2 * K**2 ),
          ( -ky * kz * vpt_t ) / k**2 + ( omega**2 * ky * kz * vpt_l ) / ( k**2 * K**2 ),
          ( -1 - ( kz**2 / k**2 ) ) * vpt_t + ( omega**2 * kz**2 * vpt_l ) / ( k**2 * K**2 ) ]])
    return a

  def construct_real_vpt(p):
      k, omega, kx, vpt_l, K, vpt_t, ky, kz = sympy.symbols('k omega k_x vpt_l K vpt_t k_y k_z')
      a = sympy.Matrix([
          # Row 1
          [ (-k**2 * vpt_l) / K**2,                          #Column 1
            (-complex(1,0) * omega * kx * vpt_l) / K**2,   #Column 2
            (-complex(1,0) * omega * ky * vpt_l) / K**2,   #Column 3 
            (-complex(1,0) * omega * kz * vpt_l) / K**2 ], #Column 4 

          # Row 2
          [ (-complex(1,0) * omega * kx * vpt_l) / K**2, 
            (-1 - ( kx**2 / k**2 ) ) * vpt_t + ( omega**2 * kx**2 * vpt_l ) / ( k**2 * K**2 ), 
            ( -kx * ky * vpt_t ) / k**2 + ( omega**2 * kx * ky * vpt_l ) / ( k**2 * K**2 ), 
            ( -kx * kz * vpt_t ) / k**2 + ( omega**2 * kx * kz * vpt_l ) / ( k**2 * K**2 ) ],

          # Row 3
          [ (-complex(1,0) * omega * ky * vpt_l) / K**2, 
            ( -kx * ky * vpt_t ) / k**2 + ( omega**2 * kx * ky * vpt_l ) / ( k**2 * K**2 ),
            ( -1 - ( ky**2/ k**2 ) ) * vpt_t + ( omega**2 * ky**2 * vpt_l ) / ( k**2 * K**2 ), 
            (( -ky * kz * vpt_t ) / k**2) + ( omega**2 * ky * kz * vpt_l ) / ( k**2 * K**2 ) ],

          # Row 4
          [ (-complex(1,0) * omega * kz * vpt_l) / k**2, 
            ( -kx * kz * vpt_t ) / k**2 + ( omega**2 * kx * kz * vpt_l ) / ( k**2 * K**2 ),
            ( -ky * kz * vpt_t ) / k**2 + ( omega**2 * ky * kz * vpt_l ) / ( k**2 * K**2 ),
            ( -1 - ( kz**2 / k**2 ) ) * vpt_t + ( omega**2 * kz**2 * vpt_l ) / ( k**2 * K**2 ) ]])
      return a

  def construct_partial_vpt(p):
      k, omega, kx, vpt_l, K, vpt_t, ky, kz = sympy.symbols('k omega k_x vpt_l K vpt_t k_y k_z')
      a = sympy.Matrix([
          # Row 1
          [ (-k**2 * vpt_l) / K**2,                          #Column 1
            1 ,   #Column 2
            1 ,   #Column 3 
            1 ], #Column 4 

          # Row 2
          [ 1, 
            1, 
            1, 
            1 ],

          # Row 3
          [ 1, 
            1,
            1, 
            1 ],

          # Row 4
          [ 1, 
            1,
            1,
            1 ]])
      return a

  def construct_reduced_vpt(p):
    k, omega, kx, vpt_l, K, vpt_t, ky, kz = sympy.symbols('k omega k_x vpt_l K vpt_t k_y k_z')
    a = sympy.Matrix([
        # Row 1
        [ 0,                          #Column 1
          0,   #Column 2
          0,   #Column 3 
          0 ], #Column 4 

        # Row 2
        [ (-complex(0,1) * omega * kx * vpt_l) / K**2, 
          (-1 - ( kx**2 / k**2 ) ) * vpt_t + ( omega**2 * kx**2 * vpt_l ) / ( k**2 * K**2 ), 
          ( -kx * ky * vpt_t ) / k**2 + ( omega**2 * kx * ky * vpt_l ) / ( k**2 * K**2 ), 
          ( -kx * kz * vpt_t ) / k**2 + ( omega**2 * kx * kz * vpt_l ) / ( k**2 * K**2 ) ],

        # Row 3
        [ (-complex(0,1) * omega * ky * vpt_l) / K**2, 
          ( -kx * ky * vpt_t ) / k**2 + ( omega**2 * kx * ky * vpt_l ) / ( k**2 * K**2 ),
          ( -1 - ( ky**2/ k**2 ) ) * vpt_t + ( omega**2 * ky**2 * vpt_l ) / ( k**2 * K**2 ), 
          (( -ky * kz * vpt_t ) / k**2) + ( omega**2 * ky * kz * vpt_l ) / ( k**2 * K**2 ) ],

        # Row 4
        [ (-complex(0,1) * omega * kz * vpt_l) / k**2, 
          ( -kx * kz * vpt_t ) / k**2 + ( omega**2 * kx * kz * vpt_l ) / ( k**2 * k**2 ),
          ( -ky * kz * vpt_t ) / k**2 + ( omega**2 * ky * kz * vpt_l ) / ( k**2 * k**2 ),
          ( -1 - ( kz**2 / k**2 ) ) * vpt_t + ( omega**2 * kz**2 * vpt_l ) / ( k**2 * k**2 ) ]])
    return a
def compute_charpoly_fullvpt():
  vpt = VacuumPolarizationTensor()
  M_vpt = vpt.construct_vpt()

  x,y,z,lamda = sympy.symbols('x y z lamda')
  #M = sympy.Matrix([[3, -2,  4, -2], [5,  3, -3, -2], [5, -2,  2, -2], [5, -2, -3,  3]])
  #M = sympy.Matrix([[3, x,  4, -2], [5,  y, -3, -2], [5, z,  2, -2], [5, -2, -3,  3]])
  print("\n\n\n Full VPT matrix")

  #sympy.pprint(sympy.latex(M_vpt))
  sympy.pprint(M_vpt)

  #print("\n\n\n Full VPT matrix Eigenvectors")
  #sympy.pprint(M.eigenvects())
  print("\n\n\n Full VPT matrix diagonalization")
  p, d = M_vpt.diagonalize()
  print("\n\n\n p matrix")
  p = sympy.simplify(p)
  print("\n\n\n d matrix")
  d = sympy.simplify(d)
  print("\n\n\n p^-1 matrix")
  sympy.pprint(sympy.simplify(p.inv()))
  print("\n\n\n satisfies p*d*p.inv() == original matrix?")
  sympy.pprint(p,d, pdpinv == M_vpt)

  #poly = M_vpt.charpoly(lamda)
  #sympy.pprint(poly)
  #print("Characteristic Polynomial \n\n\n")
  #sympy.pprint(sympy.latex(sympy.factor(poly)))
  #sympy.pprint(sympy.factor(poly))

  #sympy.pprint(M_vpt.eigenvects())

#compute_charpoly()

def compute_charpoly_onemtx():
  lamda = sympy.symbols('lamda')
  print("Simpler form of matrix")
  zero_M = sympy.ones(4,4)
  sympy.pprint(zero_M)
  sympy.pprint(sympy.factor( zero_M.charpoly(lamda) ))
  sympy.pprint(zero_M.eigenvects() )
  p,d = zero_M.diagonalize()
  sympy.pprint( (p,d, p*d*p**-1 == zero_M ) )

def compute_charpoly_simplevpt():
  #https://docs.sympy.org/latest/tutorial/matrices.html
  vpt = VacuumPolarizationTensor()
  lamda = sympy.symbols('lamda')
  print("Simpler form of matrix")
  zero_M = sympy.simplify(vpt.construct_partial_vpt())
  sympy.pprint(zero_M)
  sympy.pprint(sympy.factor( zero_M.charpoly(lamda) ))
  sympy.pprint(zero_M.eigenvects() )
  p,d = zero_M.diagonalize()
  p = sympy.simplify(p)
  d = sympy.simplify(d)
  pdpinv = sympy.simplify(p*d*p.inv())
  sympy.pprint( (p,d, pdpinv == zero_M ) )

def compute_charpoly_partialvpt():
  #https://docs.sympy.org/latest/tutorial/matrices.html
  vpt = VacuumPolarizationTensor()
  lamda = sympy.symbols('lamda')

  #partial_M = vpt.construct_reduced_vpt()
  partial_M = vpt.construct_vpt()
  partial_M.row_del(1)
  partial_M.col_del(1)
  partial_M = sympy.simplify(partial_M)
  print("\n\n\nVPT matrix without its first row and column")
  sympy.pprint(partial_M)
  #sympy.pprint(sympy.latex(partial_M))
  #print("\n\n\nCharacteristic Polynomial")
  #sympy.pprint(sympy.latex(sympy.factor( partial_M.charpoly(lamda) )))
  #sympy.pprint(sympy.factor( partial_M.charpoly(lamda) ))
  #sympy.pprint(partial_M.eigenvects() )
  p,d = partial_M.diagonalize()
  p = sympy.simplify(p)
  d = sympy.simplify(d)
  pdpinv = sympy.simplify(p*d*p.inv())

def compute_charpoly_realvpt():
  #https://docs.sympy.org/latest/tutorial/matrices.html
  vpt = VacuumPolarizationTensor()
  lamda = sympy.symbols('lamda')

  #partial_M = vpt.construct_reduced_vpt()
  partial_M = vpt.construct_real_vpt()
  partial_M = sympy.simplify(partial_M)
  print("\n\n\nReal VPT matrix")
  sympy.pprint(partial_M)
  print()
  #sympy.pprint(sympy.latex(partial_M))
  #print("\n\n\nCharacteristic Polynomial")
  #sympy.pprint(sympy.latex(sympy.factor( partial_M.charpoly(lamda) )))
  #sympy.pprint(sympy.factor( partial_M.charpoly(lamda) ))

  print("\n\n\nReal VPT matrix Eigenvectors")
  sympy.pprint(partial_M.eigenvects() )
  print()
  p,d = partial_M.diagonalize()
  p = sympy.simplify(p)
  d = sympy.simplify(d)
  pdpinv = sympy.simplify(p*d*p.inv())
  
  #too complicated to compare using simplify()
  #sympy.pprint( (p,d,pdpinv, pdpinv == partial_M ) )

  #must expand before equality test due to simplification algorithm
  print("\n\n\nReal VPT matrix: P matrix, D matrix, p^-1 matrix, is P*D*P^-1 == original matrix?")
  sympy.pprint( (p,d,pdpinv, sympy.expand(pdpinv) == sympy.expand(partial_M) ) )

  print()
  #alternate way to test if equal 
  #A - B = Matrix(0)
  #sympy.pprint( ( sympy.simplify((pdpinv - partial_M)),  (sympy.simplify((pdpinv - partial_M)) == sympy.zeros(2)) ))



#https://en.wikipedia.org/wiki/Eigenvalue_algorithm
#https://en.wikipedia.org/wiki/Diagonalizable_matrix
#compute_charpoly_simplevpt()
#compute_charpoly_partialvpt()
compute_charpoly_realvpt()
#compute_charpoly_fullvpt()


input("\n\npress any key to continue")
