import sympy

print("\nThis program will check if a given symbolic matrix is diagonizable\n")

# {{0, 0, w}, {0, t, -w}, {w, -w, r}}
w,x,y,z = sympy.symbols('w x y z')


matrix = sympy.Matrix([
  [ w, 0],
  [ y, 0 ],
])


''' Diagonizable
matrix = sympy.Matrix([
  [ w, 0],
  [ y, 0 ],
])
'''

'''
matrix = sympy.Matrix([
  [ w, z ,],
  [ y, x ],
])
'''

''' Diagonizable
matrix = sympy.Matrix([
  [ 0, 0 ,w ],
  [ 0, 0, 0 ],
  [ w, 0, 0 ]
])
'''

''' Diagonizable
matrix = sympy.Matrix([
  [ 0, 0 ,w, 0],
  [ 0, 0, 0, 0],
  [ w, 0, 0, 0],
  [ w, 0, 0, 0]
])
'''

print("\nYour matrix is:")
sympy.pprint(matrix)

detcalc = input("\nCompute determinant?: (y/n)")

if detcalc == 'y':
  sympy.pprint(matrix.det())

nullcalc = input("\nCompute nullspace?: (y/n)")

if nullcalc == 'y':
  sympy.pprint(matrix.nullspace())

ceqcalc = input("\nGenerate characteristic equation? : (y/n)")

if ceqcalc == 'y':
  lamda = sympy.symbols('lamda')
  sympy.pprint(sympy.factor(matrix.charpoly(lamda)))

evcalc = input("\nCompute eigenvalues? (computationally expensive in some cases): (y/n)")

if evcalc == 'y':
  sympy.pprint(matrix.eigenvals())

eveccalc = input("\nCompute eigenvectors? (computationally expensive in some cases): (y/n)")

if eveccalc == 'y':
  sympy.pprint(matrix.eigenvects())


dgcalc = input("\nCompute diagonalization? (computationally expensive in some cases): (y/n)")

if dgcalc == 'y':
  P, D = matrix.diagonalize()
  Pinv = P.inv()
  print("P matrix: ")
  sympy.pprint(sympy.simplify(P))
  print("D matrix: ")
  sympy.pprint(sympy.simplify(D))
  print("P^-1 matrix: ")
  sympy.pprint(sympy.simplify(Pinv))
  print("P*D*P^-1 matrix: ")
  simplifiedA = sympy.simplify(P*D*P.inv())
  sympy.pprint(simplifiedA)
  print("P*D*P^-1 == original matrix: ", ("True" if simplifiedA == matrix else "False"))
  print("So, your matrix is " + ("" if simplifiedA == matrix else "NOT ") + "diagonizable") 

input("\nDone.")

