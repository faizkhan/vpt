import sympy

print("\nThis program will check if a given matrix is diagonizable\n")
row = int(input("Enter the number of rows of the matrix:"))
column = int(input("Enter the number of columns of the matrix:"))

matrix = sympy.Matrix()
for i in range(0, row):
  col = []
  for j in range(0, column):

    el = sympy.S(input("Enter element to be placed in row " + str(i) + ", column " + str(j) + ": "));
    col += [el]

  matrix = matrix.row_insert(i, sympy.Matrix([col]))

print("\nYour matrix is:")
sympy.pprint(matrix)

detcalc = input("\nCompute determinant?: (y/n)")

if detcalc == 'y':
  sympy.pprint(matrix.det())

nullcalc = input("\nCompute nullspace?: (y/n)")

if nullcalc == 'y':
  sympy.pprint(matrix.nullspace())

ceqcalc = input("\nGenerate characteristic equation? : (y/n)")

if ceqcalc == 'y':
  lamda = sympy.symbols('lamda')
  sympy.pprint(sympy.factor(matrix.charpoly(lamda)))

evcalc = input("\nCompute eigenvalues? (computationally expensive in some cases): (y/n)")

if evcalc == 'y':
  sympy.pprint(matrix.eigenvals())

eveccalc = input("\nCompute eigenvectors? (computationally expensive in some cases): (y/n)")

if eveccalc == 'y':
  sympy.pprint(sympy.simplify(matrix.eigenvects()))


dgcalc = input("\nCompute diagonalization? (computationally expensive in some cases): (y/n)")

if dgcalc == 'y':
  P, D = matrix.diagonalize()
  print("P matrix: ")
  #sympy.pprint(P)
  sympy.pprint(sympy.simplify(P))
  print("Pinv matrix: ")
  sympy.pprint(sympy.simplify(P.inv()))
  print("D matrix: ")
  sympy.pprint(sympy.simplify(D))
  print("P*D*P^-1 matrix: ")
  simplifiedA = sympy.simplify(P*D*P.inv())
  sympy.pprint(simplifiedA)
  print("P*D*P^-1 == original matrix: ", ("True" if simplifiedA == matrix else "False"))
  print("So, your matrix is " + ("" if simplifiedA == matrix else "NOT ") + "diagonizable") 

input("\nDone.")

