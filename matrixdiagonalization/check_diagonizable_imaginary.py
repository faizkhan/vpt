import sympy

print("\nThis program will check if a given symbolic matrix is diagonizable\n")

# {{0, 0, w}, {0, t, -w}, {w, -w, r}}
w,x,y,z = sympy.symbols('w x y z')


''' Diagonizable
matrix = sympy.Matrix([
  [ complex(-1,-3), 5],
  [ -2, complex(1,-3) ],
])
'''

''' Diagonizable
matrix = sympy.Matrix([
  [ complex(-1,-3), complex(5,1)],
  [ complex(-2,1), complex(1,-3) ],
])
'''

''' Diagonizable
matrix = sympy.Matrix([
  [ complex(-1,-3), complex(5,1), complex(-1,-4)],
  [ complex(-2,1), complex(1,-3), complex(6,2)],
  [ complex(-2,1), complex(1,-3), complex(8,-9)],
])
'''
matrix = sympy.Matrix([
  [ -complex(0,1)*w, 0],
  [ -complex(0,1)*y, 0],
])

''' Diagonizable
matrix = sympy.Matrix([
  [ 0, 0 ,w, 0],
  [ 0, 0, 0, 0],
  [ w, 0, 0, 0],
  [ w, 0, 0, 0]
])
'''

print("\nYour matrix is:")
sympy.pprint(matrix)

detcalc = input("\nCompute determinant?: (y/n)")

if detcalc == 'y':
  sympy.pprint(matrix.det())

nullcalc = input("\nCompute nullspace?: (y/n)")

if nullcalc == 'y':
  sympy.pprint(matrix.nullspace())

ceqcalc = input("\nGenerate characteristic equation? : (y/n)")

if ceqcalc == 'y':
  lamda = sympy.symbols('lamda')
  sympy.pprint(sympy.factor(matrix.charpoly(lamda)))

evcalc = input("\nCompute eigenvalues? (computationally expensive in some cases): (y/n)")

if evcalc == 'y':
  sympy.pprint(matrix.eigenvals())

eveccalc = input("\nCompute eigenvectors? (computationally expensive in some cases): (y/n)")

if eveccalc == 'y':
  sympy.pprint(matrix.eigenvects())


dgcalc = input("\nCompute diagonalization? (computationally expensive in some cases): (y/n)")

if dgcalc == 'y':
  P, D = matrix.diagonalize()
  Pinv = P.inv()
  print("P matrix: ")
  sympy.pprint(P)
  #sympy.pprint(sympy.simplify(P))
  print("D matrix: ")
  #sympy.pprint(D)
  sympy.pprint(sympy.simplify(D))
  print("P^-1 matrix: ")
  sympy.pprint(Pinv)
  #sympy.pprint(sympy.simplify(Pinv))
  print("P*D*P^-1 matrix: ")
  simplifiedA = P*D*P.inv()
  #simplifiedA = sympy.simplify(P*D*P.inv())
  #sympy.pprint(sympy.expand(simplifiedA))

  diff = simplifiedA - matrix
  sympy.pprint(sympy.expand(sympy.simplify(diff)))

  #sympy.pprint(sympy.expand(sympy.simplify(diff)))

  #print("P*D*P^-1 == original matrix: ", ("True" if sympy.expand(simplifiedA) == sympy.expand(matrix) else "False"))
  #print("So, your matrix is " + ("" if sympy.expand(simplifiedA) == sympy.expand(matrix) else "NOT ") + "diagonizable") 

input("\nDone.")

