#vacuum polarization tensor simulation

import sympy as sp

sp.interactive.printing.init_printing(use_unicode=False, wrap_line=False)
#for P_uv (transverse only) tensor

kx,ky,kz,omega = sp.symbols('kx,ky,kz,omega')

k = sp.sqrt( kx**2 + ky**2 + kz**2 )
K = omega**2 - kx**2 - ky**2 - kz**2

P_uv = sp.Matrix([ 

[0,0,0,0],

[ 0, 
  (-1 - ( kx**2 / k**2 ) )  , 
  ( -kx * ky  ) / k**2  , 
  ( -kx * kz  ) / k**2   ],

[ 0, 
  ( -kx * ky ) / k**2  ,
  ( -1 - ( ky**2/ k**2 ) ) , 
  (( -ky * kz  ) / k**2)   ],

[ 0, 
  ( -kx * kz ) / k**2  ,
  ( -ky * kz ) / k**2  ,
  ( -1 - ( kz**2 / k**2 ) ) ]])

#sp.pprint(sp.latex(P_uv))
sp.pprint(P_uv)

def run_limit_tests_puv():
  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      #sp.pprint(P_uv[row,column])
      #print(sp.limit(P_uv[row,column], kx, sp.oo))
      limitmtx[row,column] = sp.limit(P_uv[row,column], kx, ky)

  print("\n Limit matrix as kx -> ky ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(P_uv[row,column], kx, kz)

  print("\n Limit matrix as kx -> kz ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(P_uv[row,column], kx, 0)

  print("\n Limit matrix as kx -> 0")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(P_uv[row,column], kx, sp.oo)

  print("\n Limit matrix as kx -> +Inf ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(P_uv[row,column], kx, -sp.oo)

  print("\n Limit matrix as kx -> -Inf")
  sp.pprint(limitmtx)

  #----------------------------


  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      #sp.pprint(P_uv[row,column])
      #print(sp.limit(P_uv[row,column], ky, sp.oo))
      limitmtx[row,column] = sp.limit(P_uv[row,column], ky, kx)

  print("\n Limit matrix as ky -> kx ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(P_uv[row,column], ky, kz)

  print("\n Limit matrix as ky -> kz ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(P_uv[row,column], ky, 0)

  print("\n Limit matrix as ky -> 0")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(P_uv[row,column], ky, sp.oo)

  print("\n Limit matrix as ky -> +Inf ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(P_uv[row,column], ky, -sp.oo)

  print("\n Limit matrix as ky -> -Inf")
  sp.pprint(limitmtx)


  #---------------------------


  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      #sp.pprint(P_uv[row,column])
      #print(sp.limit(P_uv[row,column], kz, sp.oo))
      limitmtx[row,column] = sp.limit(P_uv[row,column], kz, ky)

  print("\n Limit matrix as kz -> ky ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(P_uv[row,column], kz, kx)

  print("\n Limit matrix as kz -> kx ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(P_uv[row,column], kz, 0)

  print("\n Limit matrix as kz -> 0")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(P_uv[row,column], kz, sp.oo)

  print("\n Limit matrix as kz -> +Inf ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(P_uv[row,column], kz, -sp.oo)

  print("\n Limit matrix as kz -> -Inf")
  sp.pprint(limitmtx)

#------------------------

run_limit_tests_puv()

Q_uv = sp.Matrix([ 

[ -k**2  / K**2,
  -complex(0,1) * omega * kx / K**2,
  -complex(0,1) * omega * ky / K**2,
  -complex(0,1) * omega * kz / K**2],

[ -complex(0,1) * omega * kx / K**2, 
  ( omega**2 * kx**2 ) / ( k**2 * K**2 )  , 
  ( omega**2 * kx * ky ) / ( k**2 * K**2 ), 
  ( omega**2 * kx * kz ) / ( k**2 * K**2 ) ],

[ (-complex(0,1) * omega * ky ) / K**2, 
  ( omega**2 * kx * ky ) / ( k**2 * K**2 ) ,
  ( omega**2 * ky**2 ) / ( k**2 * K**2 ) , 
  ( omega**2 * ky * kz ) / ( k**2 * K**2 ) ],

[ (-complex(0,1) * omega * kz ) / k**2, 
  ( omega**2 * kx * kz  ) / ( k**2 * k**2 ),
  ( omega**2 * ky * kz  ) / ( k**2 * k**2 ),
  ( omega**2 * kz**2  ) / ( k**2 * k**2 )  
]])

sp.pprint(Q_uv)

def run_limit_tests_quv():
  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      #sp.pprint(P_uv[row,column])
      #print(sp.limit(P_uv[row,column], kx, sp.oo))
      limitmtx[row,column] = sp.limit(Q_uv[row,column], kx, ky)

  print("\n Limit matrix as kx -> ky ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(Q_uv[row,column], kx, kz)

  print("\n Limit matrix as kx -> kz ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(Q_uv[row,column], kx, 0)

  print("\n Limit matrix as kx -> 0")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(Q_uv[row,column], kx, sp.oo)

  print("\n Limit matrix as kx -> +Inf ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(Q_uv[row,column], kx, -sp.oo)

  print("\n Limit matrix as kx -> -Inf")
  sp.pprint(limitmtx)

  #----------------------------


  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      #sp.pprint(Q_uv[row,column])
      #print(sp.limit(Q_uv[row,column], ky, sp.oo))
      limitmtx[row,column] = sp.limit(Q_uv[row,column], ky, kx)

  print("\n Limit matrix as ky -> kx ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(Q_uv[row,column], ky, kz)

  print("\n Limit matrix as ky -> kz ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(Q_uv[row,column], ky, 0)

  print("\n Limit matrix as ky -> 0")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(Q_uv[row,column], ky, sp.oo)

  print("\n Limit matrix as ky -> +Inf ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(Q_uv[row,column], ky, -sp.oo)

  print("\n Limit matrix as ky -> -Inf")
  sp.pprint(limitmtx)


  #---------------------------


  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      #sp.pprint(Q_uv[row,column])
      #print(sp.limit(Q_uv[row,column], kz, sp.oo))
      limitmtx[row,column] = sp.limit(Q_uv[row,column], kz, ky)

  print("\n Limit matrix as kz -> ky ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(Q_uv[row,column], kz, kx)

  print("\n Limit matrix as kz -> kx ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(Q_uv[row,column], kz, 0)

  print("\n Limit matrix as kz -> 0")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(Q_uv[row,column], kz, sp.oo)

  print("\n Limit matrix as kz -> +Inf ")
  sp.pprint(limitmtx)

  limitmtx = sp.zeros(4)
  for row in range(0,4):
    for column in range(0,4):
      limitmtx[row,column] = sp.limit(Q_uv[row,column], kz, -sp.oo)

  print("\n Limit matrix as kz -> -Inf")
  sp.pprint(limitmtx)


#run_limit_tests_quv()
