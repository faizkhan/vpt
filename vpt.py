#vacuum polarization tensor simulation

#import dependencies
import scipy.integrate as integrate #integration library
import scipy.constants as spc #scientific constants
import numpy as np #matrix library
import random as rand

#options
np.set_printoptions(linewidth=1000)

#declare constants
iterations = 10000 #iteration count for infinite sums - these should converge eventually
emass = spc.physical_constants["electron mass energy equivalent in MeV"][0]

#Class that handles parameterization of model
class ModelParameters(dict):
  __getattr__ = dict.get
  __setattr__ = dict.__setitem__

#main class that deals with computation of model
class VacuumPolarizationTensor:

  #default model parameters
  lambda1 = rand.randint(300,375) * 10**-9 #nanometers
  lambda2 = rand.randint(300,375) * 10**-7 #nanometers 

  #calculate temporal angular frequency in rad/s
  omega = np.floor( (2 * spc.pi) / lambda1) 
  #calculate spatial angular frequency (wavenumber) in rad/nm
  k = np.floor( (2*spc.pi) / lambda2 )
  kx = ky = kz = np.floor(np.sqrt(k**2 / 3 ))
  #kelvin = 8 * 10**-1
  #kelvin = 1 * 10**10 #Kelvin
  kelvin=2
  beta = 1 / kelvin

  #augment kx,ky,kz with some random factor
  #kx = np.floor(k + k * (rand.randint(0,1000) / 1000))
  #ky = np.floor(k + k * (rand.randint(0,1000) / 1000))
  #kz = np.floor(k + k * (rand.randint(0,1000) / 1000))
  #TODO include check/assert that kx^2 + ky^2 + kz^2 must equal k

  #the four-wavevector K is defined as ( omega/c, kx,ky,kz) 
  #where c = v*lambda (v = temporal freq.) and omega = 2pi*v

  def __printparams(p):
    #print("\nModel parameters:\n")
    print("{:<20} {:<20} {:<20}".format(
     "omega={}".format(p.omega),
     "kelvin={}".format(p.kelvin),
     "beta={}".format(p.beta)))
    print("{:<20} {:<20} {:<20} {:<20} ".format(
     "k={}".format(p.k),
     "kx={}".format(p.kx),
     "ky={}".format(p.ky),
     "kz={}".format(p.kz)))
    print("{:<20} {:<20} {:<20} {:<20} ".format(
     "K={}".format(p.K),
     "kelvin/m_e={}".format(np.round(p.kelvin/emass,3)),
     "longitudinal={}".format(p.vpt_l),
     "transverse={}".format(p.vpt_t)))

  #initialization for objects
  def __init__(p,params=None):

    if params != None:
      p.__update(params)

    p.params = params

  #update model parameters if needed
  def __update(p,params):
    def computemodelparameters(omega=False, wavenumber=False, kelvin=False):

      #re-calculate 
      if omega:
        p.omega = np.floor( (2 * spc.pi) / p.lambda1) 

      if kelvin:
        p.beta = 1 / p.kelvin

      #if wavenumber: 
      #  p.kx = np.floor(p.k + p.k * (rand.randint(0,1000) / 1000))
      #  p.ky = np.floor(p.k + p.k * (rand.randint(0,1000) / 1000))
      #  p.kz = np.floor(p.k + p.k * (rand.randint(0,1000) / 1000))

      if wavenumber:
        p.k = np.sqrt(p.kx**2 + p.ky**2 + p.kz**2)

    #if params.lambda1:
    #  p.lambda1 = params.lambda1
    #  p.__computemodelparameters(omega=True)
    #  p.__computemodelparameters(wavenumber=True)
    #if params.lambda2:
    #  p.lambda2 = params.lambda2
    #  p.__computemodelparameters(omega=True)
    #  p.__computemodelparameters(wavenumber=True)
    if params.omega:
      p.omega = params.omega
    if params.kelvin:
      p.kelvin = params.kelvin
      computemodelparameters(kelvin=True)
    if params.k:
    #  p.k = params.k
      p.kx = params.kx
      p.ky = params.ky
      p.kz = params.kz
      computemodelparameters(wavenumber=True)


  #routine that does computation
  def compute(p): #p is the class object holding the model parameters defined above, params is given by the caller

    #TODO 
    #convert to more accurate Decimal format
    #should have some test values to check this program against
    #   known values of pi tensor shouldⁿ0 match before extended simulations are run

    #compute longitudinal component of vpt, based on formula from "QED Plasma at Finite Temperature up to Two Loops"
    def compute_longitudinal():
        return ( (4 * spc.e**2) / spc.pi**2 ) \
          * ( 1 - ( p.omega**2 / p.k**2 ) ) \
          * ( ( 1 - (p.omega / ( 2 * p.k )) \
          * np.log( (p.omega + p.k) / (p.omega - p.k) ) ) \
          * ( emass * p.a( emass * p.beta ) / p.beta - p.c( emass * p.beta ) / p.beta**2 ) + 1/4 \
          * ( 2*emass**2 - p.omega**2 + (11*p.k**2 + 37*p.omega**2) / 72 ) * p.b( emass * p.beta ) )

    #compute transverse component of vpt, based on formula from "QED Plasma at Finite Temperature up to Two Loops"
    def compute_transverse():
        return ( (2 * spc.e**2) / spc.pi**2 ) \
          * ( ( p.omega**2/p.k**2 + (1 - p.omega**2/p.k**2 )) \
          * (p.omega / (2*p.k) ) \
          * np.log( (p.omega + p.k) / (p.omega - p.k) )  \
          * ( emass * p.a( emass * p.beta ) / p.beta - p.c( emass * p.beta ) / p.beta**2 ) + 1/8 \
          * ( 2*emass**2 - p.omega**2 + (107*p.omega**2 + 131*p.k**2) / 72 ) * p.b( emass * p.beta ) )

    if p.omega != p.k:
      #compute 4-momentum of photon
      p.K = np.floor( np.sqrt(p.omega**2 - p.k**2) )

      if p.params and p.params.longitudinal == 2:
        p.vpt_l = compute_longitudinal()
      elif p.params and p.params.longitudinal == 1:
        p.vpt_l = 1 
      else:
        p.vpt_l = 0

      if p.params and p.params.transverse == 2:
        p.vpt_t = compute_transverse()
      elif p.params and p.params.transverse == 0:
        p.vpt_t = 0
      else:
        p.vpt_t = 1

      p.__printparams()

    else:
      #if omega == k, then we don't compute anything as this would cause a lot of undefined behavior
      #this branch returns the 0 tensor
      p.K = 1
      p.vpt_l = 0
      p.vpt_t = 0

    vpt = p.construct_vpt()

    #print("\nDEBUG:\n",vpt_l, vpt_t)
    print("\nVPT:\n\n", vpt)

  #compute Masoods abc functions, based on formulae from "QED Plasma at Finite Temperature up to Two Loops"
  def a( p, mb ):
    return np.log(1 + np.exp(-mb))

  def b( p, mb ):
    bsum = 0
    for n in range(1,iterations+1):
      bsum += (-1)**n * integrate.quad( lambda t: -np.exp(-t)/t, n*mb, np.inf)[0]
    return bsum

  def c( p, mb ):
    csum = 0
    for n in range(1,iterations+1):
      csum += (-1)**n * (np.exp(-n * mb)/n**2)
    return csum

  #compute vpt tensor, based on formula from "QED Plasma at Finite Temperature up to Two Loops"
  def construct_vpt(p):
    a = np.array([
        # Row 1
        [ (-p.k**2 * p.vpt_l) / p.K**2,                          #Column 1
          (-complex(0,1) * p.omega * p.kx * p.vpt_l) / p.K**2,   #Column 2
          (-complex(0,1) * p.omega * p.ky * p.vpt_l) / p.K**2,   #Column 3 
          (-complex(0,1) * p.omega * p.kz * p.vpt_l) / p.K**2 ], #Column 4 

        # Row 2
        [ (-complex(0,1) * p.omega * p.kx * p.vpt_l) / p.K**2, 
          (-1 - ( p.kx**2 / p.k**2 ) ) * p.vpt_t + ( p.omega**2 * p.kx**2 * p.vpt_l ) / ( p.k**2 * p.K**2 ), 
          ( -p.kx * p.ky * p.vpt_t ) / p.k**2 + ( p.omega**2 * p.kx * p.ky * p.vpt_l ) / ( p.k**2 * p.K**2 ), 
          ( -p.kx * p.kz * p.vpt_t ) / p.k**2 + ( p.omega**2 * p.kx * p.kz * p.vpt_l ) / ( p.k**2 * p.K**2 ) ],

        # Row 3
        [ (-complex(0,1) * p.omega * p.ky * p.vpt_l) / p.K**2, 
          ( -p.kx * p.ky * p.vpt_t ) / p.k**2 + ( p.omega**2 * p.kx * p.ky * p.vpt_l ) / ( p.k**2 * p.K**2 ),
          ( -1 - ( p.ky**2/ p.k**2 ) ) * p.vpt_t + ( p.omega**2 * p.ky**2 * p.vpt_l ) / ( p.k**2 * p.K**2 ), 
          (( -p.ky * p.kz * p.vpt_t ) / p.k**2) + ( p.omega**2 * p.ky * p.kz * p.vpt_l ) / ( p.k**2 * p.K**2 ) ],

        # Row 4
        [ (-complex(0,1) * p.omega * p.kz * p.vpt_l) / p.k**2, 
          ( -p.kx * p.kz * p.vpt_t ) / p.k**2 + ( p.omega**2 * p.kx * p.kz * p.vpt_l ) / ( p.k**2 * p.k**2 ),
          ( -p.ky * p.kz * p.vpt_t ) / p.k**2 + ( p.omega**2 * p.ky * p.kz * p.vpt_l ) / ( p.k**2 * p.k**2 ),
          ( -1 - ( p.kz**2 / p.k**2 ) ) * p.vpt_t + ( p.omega**2 * p.kz**2 * p.vpt_l ) / ( p.k**2 * p.k**2 ) ]])
    return a

