from abc_func import a,b,cdf, g
import scipy.integrate as integrate #integration library
import scipy.constants as spc #scientific constants
import matplotlib
import matplotlib.pyplot as plt
import sympy
import mpmath
import numpy as np
import scipy
import optparse as op

#matplotlib.rcParams['axes.titlesize'] = 24
matplotlib.rcParams['font.size'] = 24
#summation_max = 30 #max iterations for infinte sums
summation_max = 30 #max iterations for infinte sums
integration_max = 5 #max iterations for integrals

emass = spc.physical_constants["electron mass energy equivalent in MeV"][0]

sympy.init_printing(use_unicode=False,wrap_line=False)
def I_A6d(mu,b):
  E = sympy.Symbol('E')
  n = sympy.Symbol('n', integer=True)

  #''' from equations in the paper
  if mu < 0: #+nf (should be T > u)
    result = sympy.integrate(1 * sympy.summation( (-1)**n * sympy.exp( -n * b * (E - mu)), (n,1,summation_max)), (E, emass, sympy.oo))  #cant evaluate infinite sum so must use cutoff
  elif mu > 0: #-nf
    result = sympy.integrate(1 * sympy.summation( (-1)**n * sympy.exp( -n * b * (E + mu)), (n,1,summation_max)), (E, emass, sympy.oo)) #cant evaluate infinite sum so must use cutoff
  #'''

  ''' from various discussions
  if mu < 0: #+nf
    result = sympy.integrate(1 * (1/(1+ sympy.exp(b*(E-mu)))), (E, emass, sympy.oo)) # works
  elif mu > 0: #-nf
    result = sympy.integrate(1 * (1/(1+ sympy.exp(b*(E+mu)))), (E, emass, sympy.oo)) # works
  '''

  return (result)
  
def I_A6e(mu,b):
  E = sympy.Symbol('E')
  n = sympy.Symbol('n', integer=True)

  if mu < 0: #+nf (should be T > u)
    result = sympy.integrate((E**0/E**1) * sympy.summation( (-1)**n * sympy.exp( -n * b * (E - mu)), (n,1,summation_max)), (E, emass, sympy.oo))  #cant evaluate infinite sum so must use cutoff
  elif mu > 0: #-nf
    result = sympy.integrate((E**0/E**1) * sympy.summation( (-1)**n * sympy.exp( -n * b * (E + mu)), (n,1,summation_max)), (E, emass, sympy.oo)) #cant evaluate infinite sum so must use cutoff

  ''' from various discussions
  if mu < 0: #+nf
    result = sympy.summation( (-1)**n * sympy.integrate((E**0/E**1) * (1 / (sympy.exp(n*b*(E-mu)))), E ), (n,1,sympy.oo))
  elif mu > 0: #-nf
    result = sympy.summation( (-1)**n * sympy.integrate((E**0/E**1) * (1 / (sympy.exp(n*b*(E+mu)))), E ), (n,1,sympy.oo))
  '''

  return (result)

def I_A6e_2(mu,b):
  E = sympy.Symbol('E')
  n = sympy.Symbol('n', integer=True)

  '''
  if mu < 0: #+nf
    result = sympy.integrate(sympy.ln(mu / b) * (1 / (sympy.exp(n*b*(E-mu)))), (E, emass, sympy.oo ))
  elif mu > 0: #-nf
    result = sympy.integrate(sympy.ln(mu / b) * (1 / (sympy.exp(n*b*(E+mu)))), (E, emass, sympy.oo ))
  '''

  if mu < 0: #+nf
    result = sympy.summation( (-1)**n * sympy.integrate(sympy.ln(mu / b) * (1 / (sympy.exp(n*b*(E-mu)))), (E, emass, sympy.oo )), (n,1,sympy.oo))
  elif mu > 0: #-nf
    result = sympy.summation( (-1)**n * sympy.integrate(sympy.ln(mu / b) * (1 / (sympy.exp(n*b*(E+mu)))), (E, emass, sympy.oo )), (n,1,sympy.oo))

  #approx, error = result.euler_maclaurin(m=100,n=0)
  #return approx
  return result

def approximate_sum_method(mu,b,order):
  E = sympy.Symbol('E')
  n = sympy.Symbol('n', integer=True)
  
  if mu < 0: #+nf (should be T > u)
    result = sympy.integrate((E**order) * sympy.summation( (-1)**n * sympy.exp( -n * b * (E - mu)), (n,1,summation_max)), (E, emass, sympy.oo))  #cant evaluate infinite sum so must use cutoff
  elif mu > 0: #-nf
    result = sympy.integrate((E**order) * sympy.summation( (-1)**n * sympy.exp( -n * b * (E + mu)), (n,1,summation_max)), (E, emass, sympy.oo)) #cant evaluate infinite sum so must use cutoff

  return result

def I_A6f(mu,b):
  E = sympy.Symbol('E')
  n = sympy.Symbol('n', integer=True)
  l = sympy.Symbol('l', integer=True)
  a = 1 #order of magnitude  #sympy.Symbol('a', integer=True) #alpha
  
  if mu < 0: #+nf (should be T > u)
    result = sympy.integrate((E**0/E**2) * sympy.summation( (-1)**n * sympy.exp( -n * b * (E - mu)), (n,1,summation_max)), (E, emass, sympy.oo))  #cant evaluate infinite sum so must use cutoff
  elif mu > 0: #-nf
    result = sympy.integrate((E**0/E**2) * sympy.summation( (-1)**n * sympy.exp( -n * b * (E + mu)), (n,1,summation_max)), (E, emass, sympy.oo)) #cant evaluate infinite sum so must use cutoff

  ''' alternate method
  if mu < 0: #+nf
    result = sympy.integrate( (1/a)* (emass**(-a) - mu**(-a)) * (1 / (sympy.exp(n*b*(E-mu)))), (E, emass, sympy.oo ))
  elif mu > 0: #-nf
    result = sympy.integrate( (1/a)* (emass**(-a) - mu**(-a)) * (1 / (sympy.exp(n*b*(E+mu)))), (E, emass, sympy.oo ))
  '''

  ''' #for positive values of k (from paper appendix)
  if mu < 0:
    # x + u
    result = sympy.summation( (-1)**n * sympy.exp(-n * b * mu) * sympy.exp(-n * emass * b) * sympy.summation( ( sympy.factorial(a) / sympy.factorial(l) ) * ( (emass**l) / (n*b)**(a - l + 1) ), (l, 0, a)), (n, 1, sympy.oo))
  elif mu > 0:
    # x - u
    result = sympy.summation( (-1)**n * sympy.exp(n * b * mu) * sympy.exp(-n * emass * b) * sympy.summation( ( sympy.factorial(a) / sympy.factorial(l) ) * ( (emass**l) / (emass*b)**(a - l + 1) ), (l, 0, a)), (n, 1, sympy.oo))
  '''

  return type(result)

def approx_order0(b,mu):
  return a(emass,  beta, potential)*(1/beta)

def approx_order1(b,mu):
  firstterm = cdf(emass, beta, potential,2) / b**2
  secondterm =  ( emass / b ) * a(emass,  beta, potential)
  return firstterm + secondterm

def approx_order2(b,mu):
  firstterm = (2/b**3) * cdf(emass, beta, potential,3)
  secondterm =  ( 2*emass / b**2 ) * cdf(emass,  beta, potential, 2)
  thirdterm =  ( emass**2 / b ) * a(emass,  beta, potential)
  return firstterm + secondterm + thirdterm

def approx_order3(b,mu):
  firstterm = (cdf(emass,beta,potential,4) / b**4) 
  secondterm = (emass / b**3 ) * cdf(emass, beta, potential,3)
  thirdterm =  ( 3*emass**2 / b**2 ) * cdf(emass,  beta, potential, 2)
  fourthterm =  ( emass**3 / b ) * a(emass,  beta, potential)
  return firstterm + secondterm + thirdterm

potential = 1
temp = 10
beta = 1/temp
#print("Test Integral A6d : {} Approximation: {}".format(I_A6d(potential, beta), a(emass,  beta, potential)*(1/beta)))
#print("Test Integral A6e : {} Approximation: {}".format(I_A6e(potential, beta), b(emass,  beta, potential)))
#print("Test Integral A6e_2 : {} Approximation: {}".format(I_A6e_2(potential, beta), b(emass,  beta, -potential)))
#print("Test Integral A6f : {} Approximation: {}".format(I_A6f(potential, beta), beta* g(emass,  beta, potential)))

print("Test Integral A6d: {} Approximation: {}".format(approximate_sum_method(potential, beta, 0), approx_order0(beta,potential) ))
print("Test Integral A6c : {} Approximation: {}".format(approximate_sum_method(potential, beta, 1), approx_order1(beta,potential) ))
print("Test Integral A6b: {} Approximation: {}".format(approximate_sum_method(potential, beta, 2), approx_order2(beta,potential) ))
print("Test Integral A6a: {} Approximation: {}".format(approximate_sum_method(potential, beta, 3), approx_order3(beta,potential) ))
#print("Test Integral A6: {} Approximation: {}".format(approximate_sum_method(potential, beta, -2), "none" ))

