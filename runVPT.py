from vpt import *

#initialize VPT object
vpti = VacuumPolarizationTensor()

#compute VPT using default parameters for model 
print("\nDefault parameters:\n")
vpti.compute()

params = ModelParameters()  #create new model parameters

params.longitudinal = 1 #set pi_longitudinal to 1
params.transverse = 1 #set pi_transverse to 1

params.kelvin = 10

lambda1 = rand.randint(300,375) * 10**-9 #nanometers 
lambda2 = rand.randint(300,375) * 10**-7 #nanometers  

#compute new omega and k based on lambda1/lambda2 above
omega = np.floor( (2 * spc.pi) / lambda1)
k = np.floor( (2*spc.pi) / lambda2 )

#set omega and k for new computation
params.omega = omega
params.k = True #we want custom wavenumber

#split k into equal components kx,ky,kz
k_3 =  np.sqrt(k**2 / 3 )

#compute fuzz factor to randomize 
delta_k3 = k_3 * (rand.randint(0,1000) / 1000)

#print(k_3, k)

#set kx,ky,kz for new computation
params.kx = np.floor(k_3 - delta_k3)
params.ky = np.floor(k_3 + delta_k3/2)
params.kz = params.ky

#perform computation
print("\nCustom parameters:\n")
vpti = VacuumPolarizationTensor(params)
vpti.compute()

input("\n\nPress Return to quit...")
